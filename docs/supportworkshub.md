# Updating Supportworks Hub site


- Download and unzip FSI file
- Open pages in text editor and remove <?xml version="1.0" encoding="utf-8"?> from all pages<br>
- Using a find and replace all in the folder would work too
- Re-zip the file and save it with the same name.<br>
- Resist the urge to give it a new wacky name
- Upload the new zip file using this tool, <a href="http://bsiadmin.com/standalone/update_pages_from_zip.php" target="_blank">http://bsiadmin.com/standalone/update_pages_from_zip.php</a><br>


-  This uploads to <a href="http://staging.foundationsupportworks.com.basementsite.com/" target="_blank">staging.foundationsupportworks.com.basementsite.com/</a>
-  Open Contact Us page in CMS, add Contact Page module, and set module to display:none.
-  Check your pages for any broken links in the navigation

